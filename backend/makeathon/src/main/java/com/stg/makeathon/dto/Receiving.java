package com.stg.makeathon.dto;

import com.stg.makeathon.entity.Manager;

public class Receiving {
	private String listName;
	private Manager manager;
	public String getListName() {
		return listName;
	}
	public void setListName(String listName) {
		this.listName = listName;
	}
	public Manager getManager() {
		return manager;
	}
	public void setManager(Manager manager) {
		this.manager = manager;
	}
	
	
	
}
